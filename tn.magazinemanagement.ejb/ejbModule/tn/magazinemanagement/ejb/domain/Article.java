package tn.magazinemanagement.ejb.domain;

import java.io.Serializable;

import tn.magazinemanagement.ejb.domain.*;

import java.lang.String;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Article
 *
 */
@Entity
@Table(name="t_article")
public class Article implements Serializable {

	
	private String title;
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	
	private ArticlePK articlepk;
	@ManyToOne
	private Section section;

	public Article() {
		super();
	}   
	public ArticlePK getArticlepk() {
		return articlepk;
	}
	public void setArticlepk(ArticlePK articlepk) {
		this.articlepk = articlepk;
	}
	
	public Section getSection() {
		return section;
	}
	public void setSection(Section section) {
		this.section = section;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
   
}
