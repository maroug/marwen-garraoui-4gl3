package tn.magazinemanagement.ejb.domain;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Magazine
 *
 */
@Entity
@Table(name="t_magazine")
public class Magazine implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@Column(name="idMagazine")
	private int idMagazine;
	private String Name;
	private String format;
	
	public Magazine( String name, String format) {
		super();
		
		this.Name = name;
		this.format = format;
	}

	

	public Magazine() {
		super();
	}
	@Id
	@GeneratedValue
	public int getIdMagazine() {
		return idMagazine;
	}

	public void setIdMagazine(int idMagazine) {
		this.idMagazine = idMagazine;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
   
}
