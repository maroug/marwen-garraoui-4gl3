package tn.magazinemanagement.ejb.domain;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Redactor
 *
 */
@Entity

public class Redactor implements Serializable {

	
	private String cinRedactor;
	private String firstName;
	private String lastName;
	
	public Redactor(String cinRedactor, String firstName, String lastName) {
		super();
		this.cinRedactor = cinRedactor;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	
	private static final long serialVersionUID = 1L;

	public Redactor() {
		super();
	}   
	
	@Id
	public String getCinRedactor() {
		return this.cinRedactor;
	}

	public void setCinRedactor(String cinRedactor) {
		this.cinRedactor = cinRedactor;
	}   
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}   
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
   
}
