package tn.magazinemanagement.ejb.services.imp;

import java.util.Calendar;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.magazinemanagement.ejb.domain.Article;
import tn.magazinemanagement.ejb.domain.ArticlePK;
import tn.magazinemanagement.ejb.domain.Magazine;
import tn.magazinemanagement.ejb.domain.Redactor;
import tn.magazinemanagement.ejb.domain.Section;
import tn.magazinemanagement.ejb.services.interfaces.MagazineServicesLocal;
import tn.magazinemanagement.ejb.services.interfaces.MagazineServicesRemote;

/**
 * Session Bean implementation class MagazineServices
 */
@Stateless
@LocalBean
public class MagazineServices implements MagazineServicesRemote, MagazineServicesLocal {

    /**
     * Default constructor. 
     */
	@PersistenceContext(name="tn.magazinemanagement.server")
	private EntityManager em;
    public MagazineServices() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public boolean createMagazine(Magazine magazine) {
		
		em.persist(magazine);
		return true;
		
		
	}

	@Override
	public boolean createRedactor(Redactor redact) {
		
		em.persist(redact);
		
		return true;
	}


	@Override
	public boolean updateArticle(String title, String label) {
		Section section = findbylabel(label);
		Article article = findarticlebytitle(title);
		article.setSection(section);
		em.merge(article);
		return true;
	}

	@Override
	public List<Article> findAllArticlesByParams(String cinRedactor,
			String nameRedactor, String lastNameRedactor, String sectionLabel) {
		//List<Article>articles=em.createQuery("select e from Article e   where cinRedactor='"+cinRedactor+"' and firstName='"+nameRedactor+"' and lastNameRedactor='"+lastNameRedactor+"'").getResultList();
		return null;
	}

	@Override
	public boolean createRedactor(String cinRedactor, String firstName,
			String lastName) {
		Redactor redactor=new  Redactor();
		redactor.setCinRedactor(cinRedactor);
		redactor.setFirstName(firstName);
		redactor.setLastName(lastName);
		em.persist(redactor);
		
		
		return true;
	}

	@Override
	
		public int findmagazine(String nom) {
			Magazine magazine =(Magazine) em.createQuery("select m from Magazine m where Name='"+nom+"'").getSingleResult();
			int id = magazine.getIdMagazine();
			return id;
		}

		@Override
		public  String findredactor(String Name, String lastName) {
			Redactor redactor =(Redactor) em.createQuery("select r from Redactor r where firstName='"+Name+"'and lastName='"+lastName+"'").getSingleResult();
			String cin = redactor.getCinRedactor();
			return cin;
	
	}

		@Override
		public boolean createArticle(String nameMagazine, String nameRedactor,String lastNameRedactor, String titleArticle,
				Calendar publicationDate) {
			int idmagazine = findmagazine(nameMagazine);
			String cin = findredactor(nameRedactor, lastNameRedactor);
			Article article= new Article();
			ArticlePK article_PK = new ArticlePK();
			article_PK.setCinRedactorPK(cin);
			article_PK.setIdMagazinePK(idmagazine);
			article_PK.setPublicationDate(publicationDate);
			article.setTitle(titleArticle);
			article.setArticlepk(article_PK);
			em.persist(article);
			
			return true;
		}
		@Override
		public Section findbylabel(String label) {
			Section section = new Section();
			section =(Section) em.createQuery("select s from Section s where label='"+label+"'").getSingleResult();
			return section;
		}

		@Override
		public Article findarticlebytitle(String title) {
			Article article =(Article) em.createQuery("select a from Article a where title = '"+title+"'" ).getSingleResult();
			return article;
		}
	

}
