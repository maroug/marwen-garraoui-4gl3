package tn.magazinemanagement.ejb.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.magazinemanagement.ejb.domain.Article;

@Local
public interface servicemanagementArticleLocal {
	
	public void addArticle();
	public void deleteArticle();
	public Article findByArticle(Article article);
	public List<Article>listArticle();

}
